package com.example.readdoang.api

import com.example.readdoang.models.add_friend.AddFriendBody
import com.example.readdoang.models.add_friend.AddFriendResponse
import com.example.readdoang.models.contact.GetContactResponse
import com.example.readdoang.models.contact.NewContactResponse
import com.example.readdoang.models.find_user.FindUserBody
import com.example.readdoang.models.find_user.FindUserResponse
import com.example.readdoang.models.group.AddGroupBody
import com.example.readdoang.models.group.AddGroupResponse
import com.example.readdoang.models.group.GetGroupResponse
import com.example.readdoang.models.login.BodyLogin
import com.example.readdoang.models.logout.LogoutResponse
import com.example.readdoang.models.register.body.BodyRegisters
import com.example.readdoang.models.register.response.RegisterResponse
import com.example.readdoang.models.synchronize.SynchronizeResponse
import com.example.readdoang.models.user.Profile
import com.example.readdoang.models.user.User
import com.example.readdoang.util.AppConfig
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.http.*

interface ApiServices {

    /*register users*/
    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("${AppConfig.API_PATH_USER}/register")
    fun sendRegisterDataAsync(@Body body: BodyRegisters): Call<RegisterResponse>

    /*login users*/
    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("${AppConfig.API_PATH_USER}/login")
    fun sendLoginAsync(@Body body: BodyLogin): Call<RegisterResponse>

    /*get my profile data*/
    @GET("${AppConfig.API_PATH_USER}/me")
    fun getMyDataAsync(): Call<Profile>

    /*find user*/
    @POST(AppConfig.API_PATH_USER)
    fun sendFindUserAsync(@Body body: FindUserBody): Call<FindUserResponse>

    /*add friend*/
    @POST("${AppConfig.API_PATH_CONTACT}/me/store")
    fun sendAddFriendAsync(
        @Body body: AddFriendBody
    ): Call<AddFriendResponse>

    @GET("${AppConfig.API_PATH_CONTACT}/me")
    fun getMyContactAsync() : Call<NewContactResponse>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("${AppConfig.API_PATH_GROUP}/store")
    fun sendAddGroupDataAsync(@Body body: AddGroupBody): Call<AddGroupResponse>

    @GET(AppConfig.API_PATH_GROUP)
    fun getGroupDataAsync(): Call<SynchronizeResponse>
}