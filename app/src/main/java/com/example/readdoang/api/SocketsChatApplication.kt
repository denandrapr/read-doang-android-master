package com.example.readdoang.api

import android.app.Application
import android.util.Log
import com.example.readdoang.util.AppConfig
import io.socket.client.IO
import io.socket.client.Socket
import java.net.URISyntaxException


class SocketsChatApplication : Application() {

    private var mSocket: Socket? = null
    val socket: Socket?
        get() = mSocket

    init {
        mSocket = try {
            IO.socket(AppConfig.CHAT_SERVER_URL)
        } catch (e: URISyntaxException) {
            throw RuntimeException(e)
        }
    }
}