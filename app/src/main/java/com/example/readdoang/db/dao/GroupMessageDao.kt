package com.example.readdoang.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.readdoang.db.entity.GroupMessageEntity
import com.example.readdoang.db.entity.PersonalMessageEntity

@Dao
interface GroupMessageDao {

    @Query("SELECT * FROM group_message")
    fun getAllGroupMessage(): List<GroupMessageEntity>

    @Query("SELECT * FROM group_message WHERE idGroup = :idGroup")
    fun getMessageFrom(idGroup: String): List<GroupMessageEntity>

    @Insert
    fun insertToGroupMessage(conversation: GroupMessageEntity)

    @Query("DELETE FROM group_message")
    fun deleteAllGroupMessage()
}