package com.example.readdoang.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.readdoang.db.entity.GroupConversationEntity
import com.example.readdoang.db.entity.PersonalConversationEntity

@Dao
interface GroupConversationDao {

    @Query("SELECT * FROM group_conversation")
    fun getAllGroupConversation(): List<GroupConversationEntity>

    @Query("SELECT * FROM group_conversation WHERE idGroup = :idGroup")
    fun checkSameConversationId(idGroup: String): Boolean

    @Insert
    fun insertToGroupConversation(conversation: GroupConversationEntity)

    @Query("DELETE FROM group_conversation")
    fun deleteAllGroupConversation()
}