package com.example.readdoang.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.readdoang.db.entity.GroupByEntity

@Dao
interface GroupByDao {

    @Query("SELECT * FROM group_by")
    fun getGroupCreatedBy(): List<GroupByEntity>

    @Insert
    fun insertGroupCreatedBy(createdBy: GroupByEntity)

    @Query("DELETE FROM group_by")
    fun deleteAllGroupBy()
}