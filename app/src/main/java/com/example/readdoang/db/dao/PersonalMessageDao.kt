package com.example.readdoang.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.readdoang.db.entity.PersonalMessageEntity

@Dao
interface PersonalMessageDao {

    @Query("SELECT * FROM personal_message")
    fun getAllPersonalMessage(): List<PersonalMessageEntity>

    @Query("SELECT * FROM personal_message WHERE id_opponent = :opponent_id")
    fun getMessageFrom(opponent_id: String): List<PersonalMessageEntity>

    @Insert
    fun insertToPersonalMessage(conversation: PersonalMessageEntity)

    @Query("DELETE FROM personal_message")
    fun deleteAllPersonalMessage()
}