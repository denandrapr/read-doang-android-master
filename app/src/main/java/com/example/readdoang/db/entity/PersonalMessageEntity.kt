package com.example.readdoang.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "personal_message")
data class PersonalMessageEntity(
    @PrimaryKey(autoGenerate = true) var id_message: Int,
    var id_opponent: String,
    var type: Int,
    var messageContent: String
)