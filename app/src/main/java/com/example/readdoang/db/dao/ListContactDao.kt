package com.example.readdoang.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.readdoang.db.entity.ListContactEntity

@Dao
interface ListContactDao {

    @Query("SELECT * FROM list_contact")
    fun getListContact(): List<ListContactEntity>

    @Insert
    fun insertContact(contact: ListContactEntity)

    @Query("DELETE FROM list_contact")
    fun deleteAllContact()
}