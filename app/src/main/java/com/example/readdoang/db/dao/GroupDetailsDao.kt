package com.example.readdoang.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.readdoang.db.entity.GroupDetailsEntity

@Dao
interface GroupDetailsDao {

    @Query("SELECT * FROM group_details")
    fun getGroupDetails(): List<GroupDetailsEntity>

    @Insert
    fun insertGroupDetails(createdBy: GroupDetailsEntity)

    @Query("DELETE FROM group_details")
    fun deleteAllGroupDetails()
}