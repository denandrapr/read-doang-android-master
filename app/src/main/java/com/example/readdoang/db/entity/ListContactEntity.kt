package com.example.readdoang.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "list_contact")
data class ListContactEntity(
    @PrimaryKey(autoGenerate = true) var id: Int,
    var id_contact: String?,
    var username: String?
)