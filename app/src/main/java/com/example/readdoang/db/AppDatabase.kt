package com.example.readdoang.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.readdoang.db.dao.*
import com.example.readdoang.db.entity.*


@Database(
    entities = [
        ListContactEntity::class,
        PersonalMessageEntity::class,
        PersonalConversationEntity::class,
        GroupDetailsEntity::class,
        GroupByEntity::class,
        GroupParticipantsEntity::class,
        GroupMessageEntity::class,
        GroupConversationEntity::class],
    version = 1)

abstract class AppDatabase : RoomDatabase() {

    abstract fun listContactDao(): ListContactDao
    abstract fun personalMessageDao(): PersonalMessageDao
    abstract fun personalConversationDao(): PersonalConversationDao
    abstract fun groupByDao(): GroupByDao
    abstract fun groupDetailsDao(): GroupDetailsDao
    abstract fun groupParticipants(): GroupParticipantsDao
    abstract fun groupMessageDao(): GroupMessageDao
    abstract fun groupConversationDao(): GroupConversationDao

    companion object {
        @Volatile private var INSTANCE: AppDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context)= INSTANCE?: synchronized(LOCK){
            INSTANCE ?: buildDatabase(context).also {
                INSTANCE = it
            }
        }

        val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {

            }
        }

        private fun buildDatabase(context: Context)= Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            "db_read_doang"
        ).addMigrations(MIGRATION_1_2).build()
    }
}