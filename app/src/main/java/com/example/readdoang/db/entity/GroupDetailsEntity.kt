package com.example.readdoang.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "group_details")
data class GroupDetailsEntity(
    @PrimaryKey var idGroup: String,
    var groupDesc: String,
    var groupName: String
)