package com.example.readdoang.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "group_by")
data class GroupByEntity(
    @PrimaryKey(autoGenerate = true) var id: Int,
    var idGroup: String,
    var username: String,
    var idUser: String
)