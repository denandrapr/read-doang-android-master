package com.example.readdoang.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "group_conversation")
data class GroupConversationEntity(
    @PrimaryKey var idGroup: String,
    var groupName: String
)