package com.example.readdoang.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.readdoang.db.entity.GroupParticipantsEntity

@Dao
interface GroupParticipantsDao {

    @Query("SELECT * FROM group_participants")
    fun getGroupParticipants(): List<GroupParticipantsEntity>

    @Insert
    fun insertGroupParticipans(createdBy: GroupParticipantsEntity)

    @Query("DELETE FROM group_participants")
    fun deleteAllGroupParticipants()
}