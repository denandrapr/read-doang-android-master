package com.example.readdoang.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.readdoang.db.entity.PersonalConversationEntity

@Dao
interface PersonalConversationDao {

    @Query("SELECT * FROM personal_conversation")
    fun getAllPersonalConversation(): List<PersonalConversationEntity>

    @Query("SELECT * FROM personal_conversation WHERE id_opponent = :opponent_id")
    fun checkSameConversationId(opponent_id: String): Boolean

    @Insert
    fun insertToPersonalConversation(conversation: PersonalConversationEntity)

    @Query("DELETE FROM personal_conversation")
    fun deleteAllConversation()
}