package com.example.readdoang.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "personal_conversation")
data class PersonalConversationEntity(
    @PrimaryKey var id_opponent: String,
    var username: String
)