package com.example.readdoang.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "group_message")
data class GroupMessageEntity(
    @PrimaryKey(autoGenerate = true) var id_message: Int,
    var idGroup: String,
    var username: String,
    var type: Int,
    var messageContent: String
)