package com.example.readdoang.ui.home.contact_fragment

import android.content.Context
import com.example.readdoang.models.contact.GetContactResponse
import com.example.readdoang.models.contact.NewContactResponse

interface ContactContract {
    interface View {
        fun onDataGetContactComplete(response: NewContactResponse)
        fun onDataGetContactFailure(e: Throwable)
    }

    interface Presenter {
        fun getContactData(context: Context?)
    }

}