package com.example.readdoang.ui.home.contact_fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.readdoang.R
import com.example.readdoang.ui.home.contact_fragment.adapter.ContactViewPagerAdapter
import kotlinx.android.synthetic.main.fragment_contact.*

/**
 * A simple [Fragment] subclass.
 */
class ContactFragment : Fragment() {
//    class ContactFragment : Fragment(), ContactContract.View, Loading {

//    private lateinit var presenter: ContactContract.Presenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        /*presenter = ContactPresenter(this, this)*/
        return inflater.inflate(R.layout.fragment_contact, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mainViewPager.adapter = ContactViewPagerAdapter(childFragmentManager)
        mainTabLayout.setupWithViewPager(mainViewPager)
    }



    /*override fun onDataGetContactComplete(response: GetContactResponse) {
        val contactAdapter = ContactAdapter(response.contacts)
        recycler_view_contact.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = contactAdapter
        }
    }

    override fun onDataGetContactFailure(e: Throwable) {
        Log.d("Response ", "$e")
    }

    override fun onShowLoading() {
        progress_bar.visibility = View.VISIBLE
    }

    override fun onHideLoading() {
        progress_bar.visibility = View.GONE
    }*/
}
