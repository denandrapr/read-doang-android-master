package com.example.readdoang.ui.home.contact_fragment.page_group_contact.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.readdoang.R
import com.example.readdoang.db.entity.GroupDetailsEntity
import com.example.readdoang.db.entity.GroupParticipantsEntity
import com.example.readdoang.db.entity.ListContactEntity
import com.example.readdoang.ui.chats.group.GroupChatActivity
import com.example.readdoang.ui.chats.personal.PersonalChatActivity
import kotlinx.android.synthetic.main.item_my_contact.view.*

class GroupContactAdapter (private val details: List<GroupDetailsEntity>): RecyclerView.Adapter<GroupContactAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_my_contact, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return details.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindContact(details[position])
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        private val nameContact= view.name
        private val pictContact = view.avatar_profile

        fun bindContact(details: GroupDetailsEntity) {
            nameContact.text = details.groupName
            pictContact.loadThumbForName("", details.groupName)

            itemView.setOnClickListener {
                val i = Intent(itemView.context, GroupChatActivity::class.java)
                i.putExtra("id", details.idGroup)
                i.putExtra("name", details.groupName)
                itemView.context.startActivity(i)
            }
        }
    }
}