package com.example.readdoang.ui.splash_screen_activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.readdoang.R
import com.example.readdoang.ui.login_regist.LoginRegistActivity

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        Handler().postDelayed({

            val i = Intent(this, LoginRegistActivity::class.java)
            startActivity(i)
            finish()

        },3000)
    }
}
