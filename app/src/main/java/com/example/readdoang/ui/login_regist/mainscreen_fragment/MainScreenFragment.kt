package com.example.readdoang.ui.login_regist.mainscreen_fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.readdoang.R
import kotlinx.android.synthetic.main.fragment_main_screen.*

class MainScreenFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_main_screen, container, false)

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        actionButtons()
    }

    private fun actionButtons() {

        signUpButton.setOnClickListener {
            val navAction = MainScreenFragmentDirections.nextRegist()
            Navigation.findNavController(it).navigate(navAction)
        }

        signInButton.setOnClickListener {
            val navAction = MainScreenFragmentDirections.nextLogin()
            Navigation.findNavController(it).navigate(navAction)
        }

    }

    //    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.fragment_main_screen)
//
//        btnInit()
//    }
//
//    private fun btnInit() {
//
//        signInButton.setOnClickListener {
//            val i = Intent(this, LoginActivity::class.java)
//            startActivity(i)
//        }
//
//        signUpButton.setOnClickListener {
//            val i = Intent(this, RegisterActivity::class.java)
//            startActivity(i)
//        }
//
//    }
}
