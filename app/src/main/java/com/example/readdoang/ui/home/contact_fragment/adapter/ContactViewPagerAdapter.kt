package com.example.readdoang.ui.home.contact_fragment.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.readdoang.ui.home.contact_fragment.page_group_contact.GroupContactFragment
import com.example.readdoang.ui.home.contact_fragment.page_personal_contact.PersonalContactFragment

class ContactViewPagerAdapter (fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val pages = listOf(
        PersonalContactFragment(),
        GroupContactFragment()
    )

    // menentukan fragment yang akan dibuka pada posisi tertentu
    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getCount(): Int {
        return pages.size
    }

    // judul untuk tabs
    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> "Personal"
            else -> "Group"
        }
    }
}