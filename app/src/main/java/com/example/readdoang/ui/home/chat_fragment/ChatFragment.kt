package com.example.readdoang.ui.home.chat_fragment

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room

import com.example.readdoang.R
import com.example.readdoang.db.AppDatabase
import com.example.readdoang.ui.add_group.step_one.AddGroupActivity
import com.example.readdoang.ui.home.chat_fragment.adapter.ChatAdapter
import kotlinx.android.synthetic.main.fragment_chat.*

class ChatFragment : Fragment() {

    private var db: AppDatabase? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_chat, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        db = Room.databaseBuilder(view.context,
            AppDatabase::class.java,
            "db_read_doang")
            .allowMainThreadQueries().build()

        val contactAdapter = ChatAdapter(db!!.listContactDao().getListContact())
        recycler_chat.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = contactAdapter
        }

        (activity as AppCompatActivity).setSupportActionBar(my_toolbar)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(true)
        setHasOptionsMenu(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.add_new_group -> {
                val i = Intent(context, AddGroupActivity::class.java)
                context?.startActivity(i)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.chat_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }
}
