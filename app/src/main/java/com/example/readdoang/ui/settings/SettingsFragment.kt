package com.example.readdoang.ui.settings


import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation

import com.example.readdoang.R
import com.example.readdoang.ui.edit_profile.EditProfileActivity
import com.example.readdoang.ui.login_regist.LoginRegistActivity
import com.example.readdoang.util.SharedPreferenceManager
import kotlinx.android.synthetic.main.fragment_settings.*
import kotlinx.android.synthetic.main.fragment_settings.view.*
import kotlinx.android.synthetic.main.fragment_settings.view.card_synchronize

/**
 * A simple [Fragment] subclass.
 */
class SettingsFragment : Fragment(), SettingsContract.View {

    private lateinit var presenter: SettingsContract.Presenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_settings, container, false)
        presenter = SettingsPresenter(this)
        buttonInit(root)

        return root
    }

    private fun buttonInit(root: View) {
        root.card_logout.setOnClickListener {
            presenter.clearData(context)
        }

        root.card_synchronize.setOnClickListener {
            val navAction = SettingsFragmentDirections.destinationSynchronize()
            Navigation.findNavController(it).navigate(navAction)
        }

        root.card_edit_profile.setOnClickListener {
            val i = Intent(view?.context, EditProfileActivity::class.java)
            startActivity(i)
        }
    }

    override fun onUserLogoutComplete() {
            val i = Intent(context, LoginRegistActivity::class.java)
            startActivity(i)
            activity?.finish()
    }
}