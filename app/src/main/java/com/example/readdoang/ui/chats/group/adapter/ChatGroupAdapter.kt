package com.example.readdoang.ui.chats.group.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.readdoang.R
import com.example.readdoang.models.chat.Chats

class ChatGroupAdapter constructor(private val listViewType: List<Int>,
                                   private val listChat: List<Chats>) : RecyclerView.Adapter<ChatGroupAdapter.ViewHolder>() {

    companion object {
        const val VIEW_TYPE_MY_SELF = 1
        const val VIEW_TYPE_USER = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            VIEW_TYPE_MY_SELF -> {
                val view = layoutInflater.inflate(R.layout.item_layout_chat_my_self, null)
                ViewHolderChatItemMySelf(view)
            } VIEW_TYPE_USER -> {
                val view = layoutInflater.inflate(R.layout.item_layout_chat_other_user, null)
                ViewHolderChatItemUser(view)
            } else -> {
                val view = layoutInflater.inflate(R.layout.item_layout_chat_log, null)
                ViewHolderChatItemLog(view)
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val chat = listChat[position]
        listViewType[position].let {
            when (it) {
                VIEW_TYPE_MY_SELF -> {
                    val viewHolderChatItemMySelf = holder as ViewHolderChatItemMySelf
                    viewHolderChatItemMySelf.textViewMessage.text = chat.message
                } VIEW_TYPE_USER -> {
                    val viewHolderChatUser = holder as ViewHolderChatItemUser
                    viewHolderChatUser.textViewMessage.text = chat.message
                } else -> {
                    val viewHolderChatLog = holder as ViewHolderChatItemLog
                    viewHolderChatLog.textViewMessage.text = chat.message
                }
            }
        }
    }

    override fun getItemCount(): Int = listChat.size

    override fun getItemViewType(position: Int): Int = listViewType[position]

    open inner class ViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView)

    inner class ViewHolderChatItemMySelf constructor(itemView: View) : ViewHolder(itemView) {
        val textViewMessage: TextView = itemView.findViewById(R.id.message_body)
    }

    inner class ViewHolderChatItemUser constructor(itemView: View) : ViewHolder(itemView) {
        val textViewMessage: TextView = itemView.findViewById(R.id.message_body)
    }

    inner class ViewHolderChatItemLog constructor(itemView: View) : ViewHolder(itemView) {
        val textViewMessage: TextView = itemView.findViewById(R.id.message_body)
    }

}