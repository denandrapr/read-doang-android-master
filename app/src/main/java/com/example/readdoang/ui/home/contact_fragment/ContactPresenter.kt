package com.example.readdoang.ui.home.contact_fragment

import android.content.Context
import com.example.readdoang.api.NetworkFactory
import com.example.readdoang.util.Loading
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.await

class ContactPresenter(private val view: ContactContract.View,
                       private val load: Loading): ContactContract.Presenter {

    override fun getContactData(context: Context?) {
        load.onShowLoading()
        val services = NetworkFactory.serviceWithAuth(context!!)
        GlobalScope.launch (Dispatchers.Main){
            val request = services.getMyContactAsync()
            try {
                load.onHideLoading()
                val response = request.await()
                view.onDataGetContactComplete(response)
            } catch (e: Exception) {
                load.onHideLoading()
                view.onDataGetContactFailure(e)
            }
        }
    }
}