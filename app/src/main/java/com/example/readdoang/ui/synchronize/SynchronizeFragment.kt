package com.example.readdoang.ui.synchronize

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.example.readdoang.R
import com.example.readdoang.models.contact.NewContactResponse
import com.example.readdoang.models.synchronize.SynchronizeResponse
import com.example.readdoang.util.AppConfig
import kotlinx.android.synthetic.main.fragment_synchronize.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class SynchronizeFragment : Fragment(), SynchronizeContract.View {

    private lateinit var presenter: SynchronizeContract.Presenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_synchronize, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter = SynchronizePresenter(this)

        Log.d("asdf", "${AppConfig.getToken(view.context)}")
        buttonInit()
    }

    private fun getBitmapFromDrawable(drawable: Drawable): Bitmap {
        val bmp = Bitmap.createBitmap(
            drawable.intrinsicWidth,
            drawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bmp)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bmp
    }

    private fun buttonInit() {
        btn_sync_now.setOnClickListener {
            btn_sync_now.startAnimation()
            GlobalScope.launch {
                presenter.sendData(context)
            }
        }

        btn_sync_contact.setOnClickListener {
            btn_sync_contact.startAnimation()
            GlobalScope.launch {
                presenter.sendSyncKontak(context)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override suspend fun onUserGetListGroupComplete(response: SynchronizeResponse) {
        Log.d("Response-async", "$response")
        GlobalScope.launch (Dispatchers.Main) {
            btn_sync_now.doneLoadingAnimation(fillColor = R.color.btnPrimaryColor,
                bitmap = getBitmapFromDrawable(context?.getDrawable(R.drawable.ic_done_black_24dp)!!))
        }
    }

    override suspend fun onUserGetListGroupFailure(throwable: Throwable) {

    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override suspend fun onUserSyncKontakComplete(response: NewContactResponse) {
        Log.d("Response-async", "$response")
        GlobalScope.launch (Dispatchers.Main) {
            btn_sync_contact.doneLoadingAnimation(fillColor = R.color.btnPrimaryColor,
                bitmap = getBitmapFromDrawable(context?.getDrawable(R.drawable.ic_done_black_24dp)!!))
        }
    }

    override suspend fun onUserSyncKontakFailure(throwable: Throwable) {

    }
}
