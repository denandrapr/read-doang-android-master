package com.example.readdoang.ui.add_group.step_two

import android.content.Context
import android.util.Log
import androidx.core.text.isDigitsOnly
import com.example.readdoang.api.NetworkFactory
import com.example.readdoang.db.AppDatabase
import com.example.readdoang.db.entity.GroupByEntity
import com.example.readdoang.db.entity.GroupDetailsEntity
import com.example.readdoang.db.entity.GroupParticipantsEntity
import com.example.readdoang.models.group.AddGroupBody
import com.example.readdoang.models.group.PassingId
import com.example.readdoang.util.SharedPreferenceManager
import kotlinx.coroutines.*
import retrofit2.await
import java.sql.Types.NULL
import kotlin.coroutines.CoroutineContext

class AddGroupPresenter(private val view: AddGroupContract.View): AddGroupContract.Presenter, CoroutineScope {

    private var db: AppDatabase? = null
    private val job = Job()
    override val coroutineContext: CoroutineContext = job + Dispatchers.IO
    private lateinit var myUsername: String
    private lateinit var myId: String

    override suspend fun sendGroupBody(groupDesc: String, groupName: String, value: ArrayList<String>, values: ArrayList<PassingId>, context: Context?) {

        val randomNumber = getRandomString(10)
        val body = AddGroupBody(groupDesc, groupName, value, randomNumber)

        val services = NetworkFactory.serviceWithAuth(context!!)
        db = AppDatabase.invoke(context)

        getSharedPref(context)

        val addGroupDetails = GroupDetailsEntity(randomNumber, groupDesc, groupName)
        val addGroupBy = GroupByEntity(NULL, randomNumber, myUsername, myId)

        var i = 0
        coroutineScope {
            launch {
                val request = services.sendAddGroupDataAsync(body)
                try {
                    db!!.groupByDao().insertGroupCreatedBy(addGroupBy)
                    db!!.groupDetailsDao().insertGroupDetails(addGroupDetails)
                    while (i < value.size) {
                        val addGroupParticipants = GroupParticipantsEntity(
                            NULL,
                            randomNumber,
                            values[i].username.toString(),
                            value[i]
                        )
                        db!!.groupParticipants().insertGroupParticipans(addGroupParticipants)
                        i++
                    }
                    val response = request.await()
                    view.onDataAddGroupComplete(response, randomNumber, groupName)
                } catch (e: Exception) {
                    view.onDataAddGroupFailure(e)
                }
            }
        }
    }

    private fun getSharedPref(context: Context) {
        val sharedRef = SharedPreferenceManager.getInstance(context)
        myUsername = sharedRef.user.username.toString()
        myId = sharedRef.user._id.toString()
    }

    private fun getRandomString(length: Int) : String {
        val allowedChars = "123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
        return (1..length)
            .map { allowedChars.random() }
            .joinToString("")
    }
}