package com.example.readdoang.ui.login_regist.register_fragment

import android.content.Intent
import android.opengl.Visibility
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.readdoang.R
import com.example.readdoang.models.register.response.RegisterResponse
import com.example.readdoang.models.user.Profile
import com.example.readdoang.models.user.User
import com.example.readdoang.ui.home.MainActivity
import com.example.readdoang.util.Loading
import com.example.readdoang.util.SharedPreferenceManager
import com.example.readdoang.util.get_user.UserContract
import com.example.readdoang.util.get_user.UserPresenter
import kotlinx.android.synthetic.main.fragment_register.*
import kotlinx.android.synthetic.main.fragment_register.view.*
import kotlinx.android.synthetic.main.fragment_register.view.progress_bar

class RegisterFragment : Fragment(), RegisterContract.View, UserContract.View, Loading {

    private lateinit var presenter : RegisterContract.Presenter
    private lateinit var userPresenter : UserContract.Presenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_register, container, false)
        presenter = RegisterPresenter(this, this)
        userPresenter = UserPresenter(this, this)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonAction(view)
    }

    private fun buttonAction(root: View) {

        root.btnSignUp.setOnClickListener {
            val username: String = root.txt_username.text.toString()
            val email: String = root.txt_email.text.toString()
            val password: String = root.txt_password.text.toString()

            if (username.isEmpty() || email.isEmpty() || password.isEmpty()) {
                Toast.makeText(activity, "field tidak boleh kosong", Toast.LENGTH_SHORT).show()
            } else {
                presenter.sendData(username, email, password)
            }
        }

    }

    override fun onDataRegisterComplete(response: RegisterResponse?) {
        Log.d("Reponse : ", "$response")
        SharedPreferenceManager.getInstance(context!!.applicationContext).saveToken(response?.token)

        userPresenter.getUserData(context)
    }

    override fun onDataRegisterFailure(throwable: Throwable) {
        Log.d("Response : ", "$throwable")
        Toast.makeText(activity, "error $throwable", Toast.LENGTH_SHORT).show()
    }

    override fun onDataGetUserComplete(response: Profile?) {
        SharedPreferenceManager.getInstance(context!!.applicationContext).saveUser(response?.user!!)
        val i = Intent(context, MainActivity::class.java)
        startActivity(i)
        activity?.finish()
    }

    override fun onDataGetUserFailure(throwable: Throwable) {
        Log.d("Response : ", "$throwable")
        Toast.makeText(activity, "error $throwable", Toast.LENGTH_SHORT).show()
    }

    override fun onShowLoading() {
        progress_bar.visibility = View.VISIBLE
    }

    override fun onHideLoading() {
        progress_bar.visibility = View.GONE
    }
//
//    private fun actionBarInit(){
//        setSupportActionBar(toolbar)
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)
//        supportActionBar?.setDisplayShowHomeEnabled(true)
//        supportActionBar?.setDisplayShowTitleEnabled(false)
//    }
//
//    override fun onSupportNavigateUp(): Boolean {
//        onBackPressed()
//        return true
//    }
}
