package com.example.readdoang.ui.home.contact_fragment.page_personal_contact.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.readdoang.R
import com.example.readdoang.db.entity.ListContactEntity
import com.example.readdoang.ui.chats.personal.PersonalChatActivity
import kotlinx.android.synthetic.main.item_my_contact.view.*

class ContactAdapter (private val contact: List<ListContactEntity>): RecyclerView.Adapter<ContactAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_my_contact, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return contact.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindContact(contact[position])

//        holder.itemView.setOnClickListener {
//            Toast.makeText(holder.itemView.context, "Test", Toast.LENGTH_SHORT).show()
//        }
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        private val nameContact= view.name
        private val pictContact = view.avatar_profile

        fun bindContact(contact: ListContactEntity) {
            nameContact.text = contact.username
            pictContact.loadThumbForName("", contact.username)

            itemView.setOnClickListener {
                val i = Intent(itemView.context, PersonalChatActivity::class.java)
                i.putExtra("id", contact.id_contact)
                i.putExtra("username", contact.username)
                itemView.context.startActivity(i)
            }
        }
    }
}