package com.example.readdoang.ui.home.contact_fragment.page_personal_contact

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room

import com.example.readdoang.R
import com.example.readdoang.db.AppDatabase
import com.example.readdoang.ui.home.contact_fragment.ContactFragmentDirections
import com.example.readdoang.ui.home.contact_fragment.page_personal_contact.adapter.ContactAdapter
import kotlinx.android.synthetic.main.fragment_personal_contact.*

class PersonalContactFragment : Fragment() {

    private var db: AppDatabase? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_personal_contact, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonInit()

        db = Room.databaseBuilder(view.context, AppDatabase::class.java, "db_read_doang").allowMainThreadQueries().build()
        val contactAdapter =
            ContactAdapter(
                db!!.listContactDao().getListContact()
            )
        val a = db!!.groupByDao().getGroupCreatedBy()
        val b = db!!.groupDetailsDao().getGroupDetails()
        val c = db!!.groupParticipants().getGroupParticipants()
        Log.d("Response-groupBy", "$a")
        Log.d("Response-groupDetails", "$b")
        Log.d("Response-groupParti", "$c")

        recycler_view_contact.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = contactAdapter
        }
    }

    private fun buttonInit() {
        fab_to_add_contact.setOnClickListener {
            val navAction = ContactFragmentDirections.destinationAddFriend()
            Navigation.findNavController(it).navigate(navAction)
        }
    }
}
