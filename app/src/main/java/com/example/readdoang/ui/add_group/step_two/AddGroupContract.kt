package com.example.readdoang.ui.add_group.step_two

import android.content.Context
import com.example.readdoang.models.group.*

interface AddGroupContract {
    interface View {
        suspend fun onDataAddGroupComplete(response: AddGroupResponse?, id: String, name: String)
        suspend fun onDataAddGroupFailure(throwable: Throwable)

//        fun onDataGetGroupComplete(response: GetGroupResponse)
//        fun onDataGetGroupFailure(throwable: Throwable)
    }

    interface Presenter {
        suspend fun sendGroupBody(groupDesc: String,
                                  groupName: String,
                                  value: ArrayList<String>,
                                  values: ArrayList<PassingId>,
                                  context: Context?)
//        fun getGroupList(context: Context)
    }
}