package com.example.readdoang.ui.add_friend

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.readdoang.R
import com.example.readdoang.db.AppDatabase
//import com.example.readdoang.db.AppDatabase
import com.example.readdoang.models.add_friend.AddFriendResponse
import com.example.readdoang.models.find_user.FindUserResponse
import com.example.readdoang.util.Loading
import com.example.readdoang.util.SharedPreferenceManager
import com.github.ybq.android.spinkit.sprite.Sprite
import com.github.ybq.android.spinkit.style.DoubleBounce
import kotlinx.android.synthetic.main.fragment_add_friend.*
import kotlinx.android.synthetic.main.fragment_add_friend.view.*
import kotlinx.android.synthetic.main.fragment_add_friend.view.progress_bar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Exception


class AddFriendFragment : Fragment(), AddFriendContract.View, Loading {

    private lateinit var presenter: AddFriendContract.Presenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_add_friend, container, false)
        presenter = AddFriendPresenter(this, this)
        buttonInit(root)
        return root
    }

    private fun buttonInit(root: View) {
        root.btn_cari.setOnClickListener {
            val username = root.editText_username.text.toString()

            if (username.isEmpty()) {
                Toast.makeText(context, "field harus diisi", Toast.LENGTH_SHORT).show()
            } else {
                GlobalScope.launch {
                    presenter.sendUsername(root, username, context)
                }
            }
        }

        root.btn_add_friend.setOnClickListener {

        }
    }

    private fun buttonInit(root: View, idUser: String?, idFriend: String?, username: String?) {
        root.btn_add_friend.setOnClickListener {
            GlobalScope.launch {
                presenter.sendAddFriend(idUser, idFriend, username, context)
            }
        }
    }

    override suspend fun onDataFindUserComplete(view: View, response: FindUserResponse?) {

        if (response!!.messages.equals("User not found!", ignoreCase = true)){
            Toast.makeText(context, "Username tidak ada", Toast.LENGTH_SHORT).show()
            view.btn_add_friend.visibility = View.GONE
            view.text_username.visibility = View.GONE
            view.avatar_profile.visibility = View.GONE
        } else {
            Log.d("Respons-findUserC", "$response")
            val idFriend = response.user._id
            val username = response.user.username

            view.btn_add_friend.visibility = View.VISIBLE
            view.text_username.visibility = View.VISIBLE
            view.avatar_profile.visibility = View.VISIBLE

            view.text_username.text = username
            view.avatar_profile.loadThumbForName(response.user.avatar, username)

            val sharedRef = SharedPreferenceManager.getInstance(view.context)
            val idUser:String = sharedRef.user._id.toString()

            buttonInit(view, idUser, idFriend, username)
        }
    }

    override suspend fun onDataFindUserFailure(throwable: Throwable) {
        Log.d("Response-findUserF", "$throwable")
//        Toast.makeText(context, "username tidak ada", Toast.LENGTH_SHORT).show()
    }

    override suspend fun onDataAddFriendComplete(response: AddFriendResponse?) {
        Log.d("Response-addFriendC", "$response")
//        Toast.makeText(context, "Sukses menambah teman", Toast.LENGTH_SHORT).show()
    }

    override suspend fun onDataAddFriendFailure(throwable: Throwable) {
        Log.d("Response-addFriendF", "$throwable")
//        Toast.makeText(context, "gagal tambah", Toast.LENGTH_SHORT).show()
    }

    override fun onShowLoading() {
//        progress_bar.visibility = View.VISIBLE
    }

    override fun onHideLoading() {
        progress_bar.visibility = View.GONE
    }

}
