package com.example.readdoang.ui.chats.personal

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.example.readdoang.R
import com.example.readdoang.models.chat.Chats
import com.example.readdoang.ui.chats.group.adapter.ChatGroupAdapter
import com.example.readdoang.util.SharedPreferenceManager
import com.example.readdoang.api.SocketsChatApplication
import com.example.readdoang.db.AppDatabase
import com.example.readdoang.db.entity.PersonalConversationEntity
import com.example.readdoang.db.entity.PersonalMessageEntity
import com.example.readdoang.ui.chats.personal.adapter.ChatPersonalAdapter
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_personal_chat.*
import org.json.JSONException
import org.json.JSONObject
import java.sql.Types.NULL


class PersonalChatActivity : AppCompatActivity() {

    private lateinit var opponentId: String
    private lateinit var opponentUsername: String
    private var mSocket: Socket? = null
    private var isConnected = true
    private var myUsername = ""
    private var mTyping = false
    private val listChat = mutableListOf<Chats>()
    private val listViewType = mutableListOf<Int>()
    private val mTypingHandler = Handler()
    private var db: AppDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setBackgroundDrawableResource(R.drawable.chat_background)
        setContentView(R.layout.activity_personal_chat)

        /*get intent from contact list*/
        toGetIntent()

        /*to get shared pref data*/
        getSharedPrefData()

        /*to start socket*/
        socketStart()

        /*action bar*/
        actionBarInit()

        /*for handling button*/
        buttonInit()
        /*for handling edit text*/
//        edittextInit()

        db = Room.databaseBuilder(applicationContext,
            AppDatabase::class.java, "db_read_doang")
            .allowMainThreadQueries().build()
        val getChat = db!!.personalMessageDao().getMessageFrom(opponentId)
        var i = 0
        while (i < getChat.size) {
            listViewType.add(getChat[i].type)
            listChat.add(Chats(message = getChat[i].messageContent))
            val adapterChat = ChatPersonalAdapter(listViewType = listViewType, listChat = listChat)
            messages_view.layoutManager = LinearLayoutManager(this)
            messages_view.adapter = adapterChat
            messages_view.scrollToPosition(listChat.size - 1)
            i++
        }
    }

//    private fun edittextInit() {
//        text_message.addTextChangedListener(object : TextWatcher {
//            override fun afterTextChanged(s: Editable?) {
//
//            }
//
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
//
//            }
//
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//                if (!mTyping) {
//                    mTyping = true
//                    mSocket!!.emit("typing", myUsername)
//                }
//                mTypingHandler.removeCallbacks(onTypingTimeout)
//                mTypingHandler.postDelayed(
//                    onTypingTimeout,
//                    AppConfig.TYPING_TIMER_LENGTH.toLong()
//                )
//            }
//        })
//    }

    override fun onDestroy() {
        super.onDestroy()
        socketDestroy()
    }

    private fun socketStart () {
        val app = SocketsChatApplication()
        mSocket = app.socket
        mSocket!!.on(Socket.EVENT_CONNECT, onConnect)
        mSocket!!.on(Socket.EVENT_DISCONNECT, onDisconnect)
        mSocket!!.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
        mSocket!!.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
        mSocket!!.on("newMessage", onNewMessage)
//        mSocket!!.on("typing", onTyping)
//        mSocket!!.on("stop typing", stopTyping)
        mSocket!!.connect()
    }

    private fun socketDestroy () {
        mSocket!!.disconnect()
        mSocket!!.off(Socket.EVENT_CONNECT, onConnect)
        mSocket!!.off(Socket.EVENT_DISCONNECT, onDisconnect)
        mSocket!!.off("newMessage", onNewMessage)
//        mSocket!!.off("stop typing", stopTyping)
    }

    private fun getSharedPrefData() {
        val sharedRef = SharedPreferenceManager.getInstance(applicationContext)
        myUsername = sharedRef.user.username.toString()
    }

    private fun buttonInit() {

        send_button.setOnClickListener {
            val message = text_message.text.toString()
            mSocket!!.emit("newMessage", myUsername, message, opponentId)
            text_message.setText("")
            mTyping = false
            recyclerViewChat(message, 1)
            dbFunction(message)
        }
    }

    private fun dbFunction(message: String) {

        val sameId = db!!.personalConversationDao().checkSameConversationId(opponentId)
        if (!sameId) {
            val conversationBody = PersonalConversationEntity(opponentId, opponentUsername)
            db!!.personalConversationDao().insertToPersonalConversation(conversationBody)
        }

        val messageBody = PersonalMessageEntity(NULL, opponentId, 1, message)
        db!!.personalMessageDao().insertToPersonalMessage(messageBody)
    }

    private fun recyclerViewChat(message: String, type: Int) {
        listViewType.add(type)
        listChat.add(Chats(message = message))
        val adapterChat = ChatPersonalAdapter(listViewType = listViewType, listChat = listChat)
        messages_view.layoutManager = LinearLayoutManager(this)
        messages_view.adapter = adapterChat
        messages_view.scrollToPosition(listChat.size - 1)
    }

    private fun toGetIntent() {
        val i = intent
        opponentId = i.getStringExtra("id")!!
        opponentUsername = i.getStringExtra("username")!!
    }

    private val onConnect = Emitter.Listener {
        this.runOnUiThread() {
            Log.d("sockets", "${it.toList()}")
            if (!isConnected) {
                try {
                    mSocket!!.emit("userJoined", myUsername)
                    isConnected = true
                } catch (e: Exception) {
                    Log.d("Connected", "false $e")
                }
            }
        }
    }

    private val onDisconnect = Emitter.Listener {
        this.runOnUiThread {
            Log.i("Connected", "diconnected")
            isConnected = false
            Toast.makeText(
                this.applicationContext,
                "Disconnected", Toast.LENGTH_LONG
            ).show()
        }
    }

    private val onConnectError = Emitter.Listener {
        this.runOnUiThread {
            Log.e("socket", "Error connecting ${it[0]}")
        }
    }

    private val onNewMessage = Emitter.Listener { args ->
        this.runOnUiThread(Runnable {
            Log.d("respon ", "${args.toList()}")
            val data = args[0] as JSONObject
            val sender: String
            val content: String
            val id: String
            try {
                sender = data.getString("sender")
                content = data.getString("content")
                id = data.getString("conversationId")
            } catch (e: JSONException) {
                return@Runnable
            }

            if (myUsername != sender && id.equals(opponentId, ignoreCase = true)) {
                recyclerViewChat(content, 2)
                val messageBody = PersonalMessageEntity(NULL, opponentId, 2, content)
                db!!.personalMessageDao().insertToPersonalMessage(messageBody)
            }
        })
    }

//    private val onTyping = Emitter.Listener {
//        this.runOnUiThread(Runnable {
//            val data = it[0] as JSONObject
//            val username: String
//            username = try {
//                data.getString("username")
//            }catch (e: JSONException) {
//                return@Runnable
//            }
//
//            if (myUsername != username && !mTyping) {
//                mTyping = true
//                recyclerViewChat("Typing...", 3)
//            }
//        })
//    }

//    private val stopTyping = Emitter.Listener {
//        this.runOnUiThread(Runnable {
//            val data = it[0] as JSONObject
//
//            Log.d("respon ", "${it.toList()}")
//        })
//    }

    private fun actionBarInit(){
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = opponentUsername
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

//    private val onTypingTimeout = Runnable {
//        if (!mTyping) return@Runnable
//        mTyping = false
//        mSocket!!.emit("stop typing")
//    }
}
