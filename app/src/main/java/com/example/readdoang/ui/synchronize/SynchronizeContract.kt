package com.example.readdoang.ui.synchronize

import android.content.Context
import com.example.readdoang.models.contact.GetContactResponse
import com.example.readdoang.models.contact.NewContactResponse
import com.example.readdoang.models.synchronize.SynchronizeResponse

interface SynchronizeContract {
    interface View {
        suspend fun onUserGetListGroupComplete(response: SynchronizeResponse)
        suspend fun onUserGetListGroupFailure(throwable: Throwable)

        suspend fun onUserSyncKontakComplete(response: NewContactResponse)
        suspend fun onUserSyncKontakFailure(throwable: Throwable)
    }

    interface Presenter {
        suspend fun sendData(context: Context?)
        suspend fun sendSyncKontak(context: Context?)
    }
}