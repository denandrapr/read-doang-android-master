package com.example.readdoang.ui.home.contact_fragment.page_group_contact

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room

import com.example.readdoang.R
import com.example.readdoang.db.AppDatabase
import com.example.readdoang.ui.home.contact_fragment.page_group_contact.adapter.GroupContactAdapter
import kotlinx.android.synthetic.main.fragment_group_contact.*

class GroupContactFragment : Fragment() {

    private var db: AppDatabase? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_group_contact, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        db = Room.databaseBuilder(view.context, AppDatabase::class.java, "db_read_doang").allowMainThreadQueries().build()
        val contactAdapter =
            GroupContactAdapter(
                db!!.groupDetailsDao().getGroupDetails()
            )

        recycler_view_contact.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = contactAdapter
        }
    }
}
