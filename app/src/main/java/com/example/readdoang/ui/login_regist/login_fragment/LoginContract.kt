package com.example.readdoang.ui.login_regist.login_fragment

import com.example.readdoang.models.register.response.RegisterResponse
import com.example.readdoang.models.user.User

interface LoginContract {
    interface View {
        fun onComplete(response: RegisterResponse?)
        fun onFailure(throwable: Throwable)
    }

    interface Presenter {
        fun sendData(email: String, password: String)
    }
}