package com.example.readdoang.ui.add_group.step_one.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.readdoang.R
import com.example.readdoang.db.entity.ListContactEntity
import com.example.readdoang.util.AddContactToGroupInterface
import kotlinx.android.synthetic.main.item_add_contact_to_group.view.*
import kotlinx.android.synthetic.main.item_my_contact.view.avatar_profile
import kotlinx.android.synthetic.main.item_my_contact.view.name

class AddGroupAdapter (private val contact: List<ListContactEntity>): RecyclerView.Adapter<AddGroupAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_add_contact_to_group, parent, false))
    }

    override fun getItemCount(): Int {
        return contact.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindContact(contact[position])

    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        private val nameContact= view.name
        private val pictContact = view.avatar_profile
        private val cbChecked = view.checked_contact
        private lateinit var callback:AddContactToGroupInterface

        fun bindContact(contact: ListContactEntity) {
            callback = (itemView.context as AddContactToGroupInterface)

            nameContact.text = contact.username
            pictContact.loadThumbForName("", contact.username)

            cbChecked.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    callback.getIdFromContact(contact.id_contact, contact.username)
                } else {
                    callback.removeIdContact(contact.id_contact, contact.username)
                }
            }
        }
    }
}