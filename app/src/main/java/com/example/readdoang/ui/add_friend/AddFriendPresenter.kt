package com.example.readdoang.ui.add_friend

import android.content.Context
import android.util.Log
import android.view.View
import com.example.readdoang.api.NetworkFactory
import com.example.readdoang.db.AppDatabase
import com.example.readdoang.db.entity.ListContactEntity
import com.example.readdoang.models.add_friend.AddFriendBody
import com.example.readdoang.models.find_user.FindUserBody
import com.example.readdoang.util.Loading
import kotlinx.coroutines.*
import retrofit2.await
import java.sql.Types.NULL
import kotlin.coroutines.CoroutineContext

class AddFriendPresenter(
    private val view: AddFriendContract.View,
    private val load: Loading) : AddFriendContract.Presenter, CoroutineScope {

    private var db: AppDatabase? = null
    private val job = Job()
    override val coroutineContext: CoroutineContext = job + Dispatchers.IO

    override suspend fun sendUsername(root: View, username: String, context: Context?) {
        val body = FindUserBody(username)
        val services = NetworkFactory.serviceWithAuth(context!!)
        coroutineScope {
            launch(Dispatchers.Main.immediate) {
//                load.onShowLoading()
                val request = services.sendFindUserAsync(body)
                try {
//                    load.onHideLoading()
                    val response = request.await()
                    view.onDataFindUserComplete(root, response)
                } catch (e: Exception) {
//                    load.onHideLoading()
                    view.onDataFindUserFailure(e)
                }
            }
        }
    }

    override suspend fun sendAddFriend(idUser: String?, idFriend: String?, username: String?, context: Context?) {
        // show loading
        load.onShowLoading()

        val body = AddFriendBody(idFriend)

        val services = NetworkFactory.serviceWithAuth(context!!)

        db = AppDatabase.invoke(context)
        val contactBody = ListContactEntity(NULL, idFriend, username)

        coroutineScope {
            launch() {
                val request = services.sendAddFriendAsync(body)
                try {
                    val response = request.await()
                    db!!.listContactDao().insertContact(contactBody)
                    load.onHideLoading()
                    view.onDataAddFriendComplete(response)
                    Log.d("Responses ", "${db!!.listContactDao().getListContact()}")
                } catch (e: Exception) {
                    load.onHideLoading()
                    view.onDataAddFriendFailure(e)
                }
            }
        }
    }
}