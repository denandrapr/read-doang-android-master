package com.example.readdoang.ui.login_regist.login_fragment

import com.example.readdoang.api.NetworkFactory
import com.example.readdoang.models.login.BodyLogin
import com.example.readdoang.util.Loading
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.await
import java.lang.Exception

class LoginPresenter(private val view: LoginContract.View,
                     private val load: Loading) : LoginContract.Presenter {

    override fun sendData(email: String, password: String) {
        load.onShowLoading()
        val body = BodyLogin(email, password)
        val services = NetworkFactory.serviceNoAuth()
        GlobalScope.launch (Dispatchers.Main){
            val request = services.sendLoginAsync(body)
            try {
                load.onHideLoading()
                val response = request.await()
                view.onComplete(response)
            } catch (e: Exception) {
                load.onHideLoading()
                view.onFailure(e)
            }
        }
    }
}