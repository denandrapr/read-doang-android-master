package com.example.readdoang.ui.settings

import android.content.Context

interface SettingsContract {
    interface View {
        fun onUserLogoutComplete()
    }

    interface Presenter {
        fun clearData(context: Context?)
    }
}