package com.example.readdoang.ui.login_regist.register_fragment

import android.content.Context
import com.example.readdoang.api.NetworkFactory
import com.example.readdoang.models.register.body.BodyRegisters
import com.example.readdoang.util.Loading
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.await

class RegisterPresenter(private val view : RegisterContract.View,
                        private val load: Loading) : RegisterContract.Presenter {

    override fun sendData(username: String, email: String, password: String) {
        load.onShowLoading()
        val body = BodyRegisters(username, email, password)
        val service = NetworkFactory.serviceNoAuth()
        GlobalScope.launch(Dispatchers.Main) {
            val request= service.sendRegisterDataAsync(body)
            try {
                load.onHideLoading()
                val response = request.await()
                view.onDataRegisterComplete(response)
            } catch (e: Exception) {
                load.onHideLoading()
                view.onDataRegisterFailure(e)
            }
        }
    }

}