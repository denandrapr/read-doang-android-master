package com.example.readdoang.ui.home.profile_fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation

import com.example.readdoang.R
import com.example.readdoang.util.SharedPreferenceManager
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*

class ProfileFragment : Fragment() {

    private lateinit var myUsername: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getSharedPrefData(view)

        buttonInit()
        profileInit(view)
    }

    private fun profileInit(view: View) {
        txtName.text = myUsername
        view.imageView3.loadThumbForName("", myUsername)
    }

    private fun buttonInit() {

        settingButton.setOnClickListener {
            val navAction = ProfileFragmentDirections.nextDestination()
            Navigation.findNavController(it).navigate(navAction)
        }
    }

    private fun getSharedPrefData(view: View) {
        val sharedRef = SharedPreferenceManager.getInstance(view.context)
        myUsername = sharedRef.user.username.toString()
    }
}
