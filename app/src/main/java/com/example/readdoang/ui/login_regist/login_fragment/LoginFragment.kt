package com.example.readdoang.ui.login_regist.login_fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.readdoang.R
import com.example.readdoang.models.register.response.RegisterResponse
import com.example.readdoang.models.user.Profile
import com.example.readdoang.ui.home.MainActivity
import com.example.readdoang.util.Loading
import com.example.readdoang.util.SharedPreferenceManager
import com.example.readdoang.util.get_user.UserContract
import com.example.readdoang.util.get_user.UserPresenter
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_login.view.*

class LoginFragment : Fragment(), LoginContract.View, UserContract.View, Loading {

    private lateinit var presenter: LoginContract.Presenter
    private lateinit var userPresenter : UserContract.Presenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_login, container, false)
        presenter = LoginPresenter(this, this)
        userPresenter = UserPresenter(this, this)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        actionButtons(view)
    }

    private fun actionButtons(view: View) {
        view.btnSignIn.setOnClickListener {
            val email = view.txt_email.text.toString()
            val password = view.txt_password.text.toString()

            if (email.isEmpty() || password.isEmpty()) {
                Toast.makeText(activity, "field tidak boleh kosong", Toast.LENGTH_SHORT).show()
            } else {
                presenter.sendData(email, password)
            }
        }
    }

    override fun onComplete(response: RegisterResponse?) {
        SharedPreferenceManager.getInstance(context!!.applicationContext).saveToken(response!!.token)

        userPresenter.getUserData(context)
    }

    override fun onFailure(throwable: Throwable) {
        Toast.makeText(activity, "Gagal login", Toast.LENGTH_SHORT).show()
    }

    override fun onDataGetUserComplete(response: Profile?) {
        SharedPreferenceManager.getInstance(context!!.applicationContext).saveUser(response?.user!!)
        Toast.makeText(activity, "Selamat datang ${response.user.username}", Toast.LENGTH_SHORT).show()
        val i = Intent(context, MainActivity::class.java)
        startActivity(i)
        activity?.finish()
    }

    override fun onDataGetUserFailure(throwable: Throwable) {
        Toast.makeText(activity, "Gagal login", Toast.LENGTH_SHORT).show()
    }

    override fun onShowLoading() {
        progress_bar.visibility = View.VISIBLE
    }

    override fun onHideLoading() {
        progress_bar.visibility = View.GONE
    }

//    private fun buttonAction() {
//        btnSignIn.setOnClickListener {
//            val i = Intent(this, MainActivity::class.java)
//            startActivity(i)
//        }
//    }
//
//    private fun actionBarInit(){
//        setSupportActionBar(toolbar)
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)
//        supportActionBar?.setDisplayShowHomeEnabled(true)
//        supportActionBar?.setDisplayShowTitleEnabled(false)
//    }
//
//    override fun onSupportNavigateUp(): Boolean {
//        onBackPressed()
//        return true
//    }
}
