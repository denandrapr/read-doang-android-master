package com.example.readdoang.ui.add_group.step_two

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.example.readdoang.R
import com.example.readdoang.db.AppDatabase
import com.example.readdoang.db.entity.GroupByEntity
import com.example.readdoang.db.entity.GroupDetailsEntity
import com.example.readdoang.db.entity.GroupParticipantsEntity
import com.example.readdoang.models.group.*
import com.example.readdoang.ui.add_group.step_two.adapter.AddGroupDetailAdapter
import com.example.readdoang.ui.chats.group.GroupChatActivity
import com.example.readdoang.util.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_add_group_details.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.sql.Types
import java.sql.Types.NULL

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class AddGroupDetailsActivity : AppCompatActivity(), AddGroupContract.View {

    private var value: ArrayList<PassingId> = ArrayList()
    private var valueOnlyId: ArrayList<String> = ArrayList()
    private lateinit var presenter: AddGroupContract.Presenter
    private var db: AppDatabase? = null
    private lateinit var myUsername: String
    private lateinit var myId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_group_details)

        presenter = AddGroupPresenter(this)
        db = Room.databaseBuilder(applicationContext, AppDatabase::class.java, "db_read_doang").allowMainThreadQueries().build()

        getSharedPref()
        toolbarInit()
        getIntentInit()
        recyclerViewInit()
        buttonInit()
    }

    private fun buttonInit() {

        floating_button_submit.setOnClickListener {
            val groupName = txtGroupName.text.toString()
            val groupDesc = txtGroupDesc.text.toString()

            if (groupName.isEmpty() || groupDesc.isEmpty()) {
                Toast.makeText(applicationContext, "Form wajib diisi!", Toast.LENGTH_SHORT).show()
            } else {
                // do something
                loadingLayout.visibility = View.VISIBLE
                GlobalScope.launch {
                    presenter.sendGroupBody(groupDesc, groupName, valueOnlyId, value, applicationContext)
                }
            }
        }
    }

    private fun recyclerViewInit() {
        val addGroupDetailsAdapter = AddGroupDetailAdapter(value)
        recycler_selected.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = addGroupDetailsAdapter
        }
    }

    private fun getIntentInit() {
        value = intent.getParcelableArrayListExtra("passValue")
        valueOnlyId = intent.getStringArrayListExtra("passingOnlyId")
    }

    private fun toolbarInit() {

        setSupportActionBar(my_toolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {

        onBackPressed()
        return true
    }

    override suspend fun onDataAddGroupComplete(response: AddGroupResponse?, id: String, name: String) {
        Log.d("Response-add-group", "$response")
        GlobalScope.launch (Dispatchers.Main) {
            loadingLayout.visibility = View.GONE
            val i = Intent(applicationContext, GroupChatActivity::class.java)
            i.putExtra("id", id)
            i.putExtra("name", name)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
        }
    }

    override suspend fun onDataAddGroupFailure(throwable: Throwable) {
        Log.d("Response-add-group", "$throwable")
    }

    private fun getSharedPref() {
        val sharedRef = SharedPreferenceManager.getInstance(applicationContext)
        myUsername = sharedRef.user.username.toString()
        myId = sharedRef.user._id.toString()
    }

//    override fun onDataGetGroupComplete(response: GetGroupResponse) {
//
//    }
//
//    override fun onDataGetGroupFailure(throwable: Throwable) {
//
//    }
}
