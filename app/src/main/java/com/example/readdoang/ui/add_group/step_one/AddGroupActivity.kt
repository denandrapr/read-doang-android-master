package com.example.readdoang.ui.add_group.step_one

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.example.readdoang.R
import com.example.readdoang.db.AppDatabase
import com.example.readdoang.models.group.PassingId
import com.example.readdoang.models.group.PassingOnlyId
import com.example.readdoang.ui.add_group.step_one.adapter.AddGroupAdapter
import com.example.readdoang.ui.add_group.step_two.AddGroupDetailsActivity
import com.example.readdoang.util.AddContactToGroupInterface
import kotlinx.android.synthetic.main.activity_add_group.*

class AddGroupActivity : AppCompatActivity(), AddContactToGroupInterface {

    private var db: AppDatabase? = null
    private var passedId:ArrayList<PassingId> = ArrayList()
    private var passedOnlyId: ArrayList<String> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_group)

        db = Room.databaseBuilder(applicationContext, AppDatabase::class.java, "db_read_doang").allowMainThreadQueries().build()
        val contactAdapter = AddGroupAdapter(db!!.listContactDao().getListContact())
        recycler_contact.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = contactAdapter
        }

        toolbarInit()
        buttonInit()
    }

    private fun buttonInit() {

        btn_next.setOnClickListener {
            val i = Intent(applicationContext, AddGroupDetailsActivity::class.java)
            i.putParcelableArrayListExtra("passValue", ArrayList(passedId))
            i.putStringArrayListExtra("passingOnlyId", ArrayList(passedOnlyId))
            startActivity(i)
        }
    }

    private fun toolbarInit() {

        setSupportActionBar(my_toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {

        onBackPressed()
        return true
    }

    override fun getIdFromContact(id: String?, username: String?) {

        val body = PassingId(id, username)

        passedId.add(body)
        passedOnlyId.add(id!!)

        Log.d("Response-group ", "$passedId")

        if (passedId.isNotEmpty()) {
            btn_next.visibility = View.VISIBLE
        }
    }

    override fun removeIdContact(id: String?, username: String?) {

        val body = PassingId(id, username)
        val bodyWithoutUsername = PassingOnlyId(id)

        passedId.remove(body)
        passedOnlyId.remove(id)

        Log.d("Response-group ", "$passedId")
        if (passedId.isEmpty()) {
            btn_next.visibility = View.GONE
        }
    }
}
