package com.example.readdoang.ui.settings

import android.content.Context
import com.example.readdoang.api.NetworkFactory
import com.example.readdoang.db.AppDatabase
import com.example.readdoang.util.SharedPreferenceManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Exception

class SettingsPresenter(private val view: SettingsContract.View) : SettingsContract.Presenter {

    private lateinit var sharedRef : SharedPreferenceManager
    private var db: AppDatabase? = null

    override fun clearData(context: Context?) {

        //remove shared pref data
        sharedRef = SharedPreferenceManager.getInstance(context!!)
        sharedRef.clear()

        //remove room data
        db = AppDatabase.invoke(context)
        GlobalScope.launch {
            db!!.listContactDao().deleteAllContact()
            db!!.personalConversationDao().deleteAllConversation()
            db!!.personalMessageDao().deleteAllPersonalMessage()
            db!!.groupByDao().deleteAllGroupBy()
            db!!.groupDetailsDao().deleteAllGroupDetails()
            db!!.groupParticipants().deleteAllGroupParticipants()
        }

        view.onUserLogoutComplete()
    }
}