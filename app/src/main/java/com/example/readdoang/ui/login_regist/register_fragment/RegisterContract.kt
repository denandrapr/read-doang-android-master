package com.example.readdoang.ui.login_regist.register_fragment

import com.example.readdoang.models.register.response.RegisterResponse
import com.example.readdoang.models.user.Profile
import android.content.Context
import com.example.readdoang.models.user.User

interface RegisterContract {
    interface View {
        fun onDataRegisterComplete(response: RegisterResponse?)
        fun onDataRegisterFailure(throwable: Throwable)
    }

    interface Presenter {
        fun sendData(username: String, email: String, password: String)
    }
}