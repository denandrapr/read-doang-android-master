package com.example.readdoang.ui.synchronize

import android.content.Context
import android.util.Log
import com.example.readdoang.api.NetworkFactory
import com.example.readdoang.db.AppDatabase
import com.example.readdoang.db.entity.GroupByEntity
import com.example.readdoang.db.entity.GroupDetailsEntity
import com.example.readdoang.db.entity.GroupParticipantsEntity
import com.example.readdoang.db.entity.ListContactEntity
import kotlinx.coroutines.*
import retrofit2.await
import java.sql.Types.NULL
import kotlin.coroutines.CoroutineContext

class SynchronizePresenter(private val view: SynchronizeContract.View) : SynchronizeContract.Presenter, CoroutineScope {

    private var db: AppDatabase? = null
    private val job = Job()
    override val coroutineContext: CoroutineContext = job + Dispatchers.IO

    override suspend fun sendData(context: Context?) {

        val services = NetworkFactory.serviceWithAuth(context!!)
        db = AppDatabase.invoke(context)
        coroutineScope {
            launch {
                val request = services.getGroupDataAsync()
                try {
                    val response = request.await()
                    val getGroupDetails = db!!.groupDetailsDao().getGroupDetails()


                    if (getGroupDetails.isEmpty()) {
                        var i = 0
                        Log.d("coba", "kosong")
                        while (i < response.groupChat.size) {
                            val unameCreateBy = response.groupChat[i].createdBy.username
                            val idUserCreatedBy = response.groupChat[i].createdBy._id
                            val idGroup = response.groupChat[i].idGroup
                            val groupDesc = response.groupChat[i].description
                            val groupName = response.groupChat[i].groupName

                            val groupDetails = GroupDetailsEntity(idGroup, groupDesc, groupName)
                            val groupBy = GroupByEntity(NULL, idGroup, unameCreateBy, idUserCreatedBy)

                            var k = 0

                            while (k < response.groupChat[i].participants.size) {
                                val unameParticipant = response.groupChat[i].participants[k].username
                                val idParticipant = response.groupChat[i].participants[k]._id
                                val groupParticipant = GroupParticipantsEntity(NULL, idGroup, idParticipant, unameParticipant)
                                db!!.groupParticipants().insertGroupParticipans(groupParticipant)
                                k++
                            }
                            db!!.groupDetailsDao().insertGroupDetails(groupDetails)
                            db!!.groupByDao().insertGroupCreatedBy(groupBy)
                            i++
                        }
                    } else {
                        var i = 0
                        while (i < getGroupDetails.size) {
                            var j = 0
                            for (s in response.groupChat) {
                                if (s.idGroup.contains(getGroupDetails[i].idGroup, ignoreCase = true)) {
                                    Log.d("coba-2", "i = $i and j = $j and ${s.groupName}")
                                }else {
                                    if (i == 0) {
                                        Log.d("coba-3", "i = $i and j = $j and ${s.groupName}")
                                        val unameCreateBy = response.groupChat[j].createdBy.username
                                        val idUserCreatedBy = response.groupChat[j].createdBy._id
                                        val idGroup = response.groupChat[j].idGroup
                                        val groupDesc = response.groupChat[j].description
                                        val groupName = response.groupChat[j].groupName

                                        val groupDetails = GroupDetailsEntity(idGroup, groupDesc, groupName)
                                        val groupBy = GroupByEntity(NULL, idGroup, unameCreateBy, idUserCreatedBy)

                                        var k = 0

                                        while (k < response.groupChat[j].participants.size) {
                                            val unameParticipant = response.groupChat[j].participants[k].username
                                            val idParticipant = response.groupChat[j].participants[k]._id
                                            val groupParticipant = GroupParticipantsEntity(NULL, idGroup, idParticipant, unameParticipant)
                                            db!!.groupParticipants().insertGroupParticipans(groupParticipant)
                                            k++
                                        }
                                        db!!.groupDetailsDao().insertGroupDetails(groupDetails)
                                        db!!.groupByDao().insertGroupCreatedBy(groupBy)
                                    }
                                }
                                j++
                            }
                            i++
                        }
                    }

                    view.onUserGetListGroupComplete(response)
                } catch (e: Exception) {
                    view.onUserGetListGroupFailure(e)
                }
            }
        }
    }

    override suspend fun sendSyncKontak(context: Context?) {
        val services = NetworkFactory.serviceWithAuth(context!!)
        db = AppDatabase.invoke(context)
        coroutineScope {
            launch {
                val request = services.getMyContactAsync()
                try {
                    val response = request.await()
//                    val getGroupDetails = db!!.listContactDao().getListContact()
                    var i = 0
                    for (i in response.contact.listContacts) {
                        val contact = ListContactEntity(NULL, i._id, i.username)
                        db!!.listContactDao().insertContact(contact)
                    }
                    view.onUserSyncKontakComplete(response)
                } catch (e: Exception) {
                    view.onUserSyncKontakFailure(e)
                }
            }
        }
    }
}