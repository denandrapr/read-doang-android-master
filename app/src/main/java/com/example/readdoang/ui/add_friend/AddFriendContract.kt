package com.example.readdoang.ui.add_friend

import android.content.Context
import com.example.readdoang.models.add_friend.AddFriendResponse
import com.example.readdoang.models.find_user.FindUserResponse

interface AddFriendContract {

    interface View {
        suspend fun onDataFindUserComplete(view: android.view.View, response: FindUserResponse?)
        suspend fun onDataFindUserFailure(throwable: Throwable)

        suspend fun onDataAddFriendComplete(response: AddFriendResponse?)
        suspend fun onDataAddFriendFailure(throwable: Throwable)
    }

    interface Presenter {
        suspend fun sendUsername(root: android.view.View, username: String, context: Context?)
        suspend fun sendAddFriend(idUser: String?, idFriend: String?, username: String?, context: Context?)
    }
}