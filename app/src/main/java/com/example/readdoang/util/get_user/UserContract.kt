package com.example.readdoang.util.get_user

import com.example.readdoang.models.register.response.RegisterResponse
import com.example.readdoang.models.user.Profile
import android.content.Context
import com.example.readdoang.models.user.User

interface UserContract {
    interface View {
        fun onDataGetUserComplete(response: Profile?)
        fun onDataGetUserFailure(throwable: Throwable)
    }

    interface Presenter {
        fun getUserData(context: Context?)
    }
}