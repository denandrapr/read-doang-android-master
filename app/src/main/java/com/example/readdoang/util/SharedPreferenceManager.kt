package com.example.readdoang.util

import android.content.Context
import com.example.readdoang.models.user.User

class SharedPreferenceManager private constructor(
    private val context : Context){

    val isLoggedIn: Boolean
        get() {
            val sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return sharedPreferences.getString("id", "-1") != "-1"
        }

    val getToken: String?
        get() {
            val sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return sharedPreferences.getString("token", "null")
        }

    fun saveToken(token: String?){
        val sharedPreference = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreference.edit()

        editor.putString("token", token)

        editor.apply()
    }

    fun saveUser(user: User){
        val sharedPreference = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreference.edit()

        editor.putString("id", user._id)
        editor.putString("createdAt", user.createdAt)
        editor.putString("email", user.email)
        editor.putString("username", user.username)
        editor.putString("password", user.password)
        editor.putString("updatedAt", user.updatedAt)

        editor.apply()
    }

    val user: User
        get() {
            val sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return User(
                sharedPreferences.getString("id", null),
                sharedPreferences.getString("createdAt", null),
                sharedPreferences.getString("email", null),
                sharedPreferences.getString("password", null),
                sharedPreferences.getString("updatedAt", null),
                sharedPreferences.getString("username", null)
            )
        }

    fun clear() {
        val sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }

    companion object {
        private const val SHARED_PREF_NAME = "read_doang_pref"
        private var mInstance: SharedPreferenceManager? = null
        @Synchronized
        fun getInstance(mContext: Context): SharedPreferenceManager {
            if (mInstance == null) {
                mInstance = SharedPreferenceManager(mContext)
            }
            return mInstance as SharedPreferenceManager
        }
    }

}