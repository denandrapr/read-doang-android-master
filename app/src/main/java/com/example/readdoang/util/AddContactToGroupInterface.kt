package com.example.readdoang.util

interface AddContactToGroupInterface {
    fun getIdFromContact(id: String?, username: String?)
    fun removeIdContact(id: String?, username: String?)
}