package com.example.readdoang.util

import android.content.Context

object AppConfig {

    const val BASE_API_URL = "http://128.199.160.243:3000"
    const val CHAT_SERVER_URL = "http://128.199.160.243:4000"
    const val API_PATH_USER = "/api/users"
    const val API_PATH_CONTACT = "/api/contact"
    const val API_PATH_GROUP = "/api/group"

    const val TAG = "Response"
    const val TYPING_TIMER_LENGTH = 600

    fun getToken(context: Context): String? {
        return SharedPreferenceManager.getInstance(context).getToken
    }
}