package com.example.readdoang.util

interface Loading {
    fun onShowLoading()
    fun onHideLoading()
}