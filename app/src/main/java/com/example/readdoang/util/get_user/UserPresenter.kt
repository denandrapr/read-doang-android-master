package com.example.readdoang.util.get_user

import android.content.Context
import com.example.readdoang.api.NetworkFactory
import com.example.readdoang.util.Loading
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.await

class UserPresenter(private val view : UserContract.View,
                    private val load: Loading) : UserContract.Presenter {

    override fun getUserData(context: Context?) {
        load.onShowLoading()
        val service = NetworkFactory.serviceWithAuth(context!!)
        GlobalScope.launch(Dispatchers.Main) {
            val request= service.getMyDataAsync()
            try {
                load.onHideLoading()
                val response = request.await()
                view.onDataGetUserComplete(response)
            } catch (e: Exception) {
                load.onHideLoading()
                view.onDataGetUserFailure(e)
            }
        }
    }

}