package com.example.readdoang.models.group

data class GetGroupResponse(
    val groupChat: List<GroupChat>,
    val messages: String,
    val status: Int,
    val success: Boolean
)