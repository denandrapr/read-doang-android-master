package com.example.readdoang.models.synchronize

data class SynchronizeResponse(
    val groupChat: List<GroupChat>,
    val messages: String,
    val status: Int,
    val success: Boolean
)