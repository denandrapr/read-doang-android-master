package com.example.readdoang.models.register.response

data class RegisterResponse(
    val messages: String,
    val status: Int,
    val success: Boolean,
    val token: String
)