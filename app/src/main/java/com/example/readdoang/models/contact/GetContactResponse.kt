package com.example.readdoang.models.contact

data class GetContactResponse(
    val contacts: List<Contact>,
    val messages: String,
    val status: Int,
    val success: Boolean
)