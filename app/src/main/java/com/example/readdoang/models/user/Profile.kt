package com.example.readdoang.models.user

data class Profile(
    val messages: String,
    val status: Int,
    val success: Boolean,
    val user: User
)