package com.example.readdoang.models.synchronize

data class Participant(
    val _id: String,
    val about: String,
    val avatar: Any,
    val username: String
)