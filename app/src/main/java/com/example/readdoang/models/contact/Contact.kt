package com.example.readdoang.models.contact

data class Contact(
    val _id: String,
    val username: String,
    val avatar: String?
)