package com.example.readdoang.models.group

data class CreatedBy(
    val _id: String,
    val about: String,
    val avatar: Any,
    val username: String
)