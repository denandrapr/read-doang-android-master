package com.example.readdoang.models.group

import com.squareup.moshi.Json

data class AddGroupBody(
    @Json(name = "description")
    val description: String,
    @Json(name = "groupName")
    val groupName: String,
    @Json(name = "participants")
    val participants: List<String>,
    @Json(name = "idGroup")
    val idGroup: String
)