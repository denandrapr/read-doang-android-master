package com.example.readdoang.models.add_friend

data class AddFriendBody(
    val userId: String?
)