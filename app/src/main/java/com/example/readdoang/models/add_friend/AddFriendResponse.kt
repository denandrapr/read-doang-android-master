package com.example.readdoang.models.add_friend

data class AddFriendResponse(
    val messages: String,
    val status: Int,
    val success: Boolean
)