package com.example.readdoang.models.find_user

data class FindUserResponse(
    val messages: String,
    val status: Int,
    val success: Boolean,
    val user: User
)