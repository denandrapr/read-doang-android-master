package com.example.readdoang.models.logout

data class LogoutResponse(
    val message: String
)