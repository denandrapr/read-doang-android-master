package com.example.readdoang.models.contact

data class NewContactResponse(
    val contact: ContactX,
    val messages: String,
    val status: Int,
    val success: Boolean
)