package com.example.readdoang.models.synchronize

data class GroupChat(
    val _id: String,
    val avatar: Any,
    val createdAt: String,
    val createdBy: CreatedBy,
    val description: String,
    val groupName: String,
    val idGroup: String,
    val participants: List<Participant>,
    val updatedAt: String
)