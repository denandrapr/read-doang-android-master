package com.example.readdoang.models.user

data class User(
    val _id: String?,
    val createdAt: String?,
    val email: String?,
    val password: String?,
    val updatedAt: String?,
    val username: String?
)