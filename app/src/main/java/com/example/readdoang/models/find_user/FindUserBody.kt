package com.example.readdoang.models.find_user

data class FindUserBody (
    val username: String
)