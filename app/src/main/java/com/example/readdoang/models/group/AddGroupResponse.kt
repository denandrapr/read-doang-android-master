package com.example.readdoang.models.group

data class AddGroupResponse(
    val messages: String,
    val status: Int,
    val success: Boolean
)