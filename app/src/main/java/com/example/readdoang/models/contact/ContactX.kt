package com.example.readdoang.models.contact

data class ContactX(
    val _id: String,
    val createdAt: String,
    val listContacts: List<Contacts>,
    val ownerId: String,
    val updatedAt: String
)