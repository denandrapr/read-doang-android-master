package com.example.readdoang.models.find_user

data class User(
    val _id: String,
    val username: String,
    val avatar: String
)