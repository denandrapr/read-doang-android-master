package com.example.readdoang.models.group

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PassingOnlyId(
    val id: String?
) : Parcelable