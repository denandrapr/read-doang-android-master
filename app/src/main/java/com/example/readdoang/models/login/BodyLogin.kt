package com.example.readdoang.models.login

data class BodyLogin (
    val email : String?,
    val password : String?
)