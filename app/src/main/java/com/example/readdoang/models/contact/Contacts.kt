package com.example.readdoang.models.contact

data class Contacts(
    val _id: String,
    val about: String,
    val avatar: Any,
    val username: String
)