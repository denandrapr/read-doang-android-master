package com.example.readdoang.models.group

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PassingId(
    val id: String?,
    val username: String?
) : Parcelable