package com.example.readdoang.models.synchronize

data class CreatedBy(
    val _id: String,
    val about: String,
    val avatar: Any,
    val username: String
)