package com.example.readdoang.models.register.body

data class BodyRegisters(
    val username: String?,
    val email: String?,
    val password: String?
)